/**
 * 后台管理公共头
 */
import React from 'react';

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    goBack = ()=>{
        this.context.router.goBack();
    };

    render() {
        return (
            <div className="header">
                <span>{this.props.title}</span>
                {this.props.back ? <span style={backStyle} onClick={this.goBack}>
                    &lt;返回
                </span> : null}
            </div>
        );
    }
}

var backStyle = {
    cursor:'pointer',
    float:'right',
    marginRight:'10px'
};

Header.propTypes = {
    title:React.PropTypes.string,
    back:React.PropTypes.bool
};

Header.defaultProps = {
    title:'这是一个标题',
    back:false
};

Header.contextTypes = {
    router: React.PropTypes.object
};

export default Header;