export default function(component) {
    return React.createClass({
        getInitialState:function() {
            return {
                instance:null
            };
        },
        componentDidMount:function() {
            component(function(instance) {
                this.setState({
                    instance:instance
                });
            }.bind(this));
        },
        render:function() {
            if(this.state.instance) {
                var Instance = this.state.instance.default;
                return <Instance {...this.props} />;
            }else {
                return (
                    <div style={{textAlign:'center',fontSize:'24px',paddingTop:'200px'}}>
                        加载中...
                    </div>
                );
            }
        }
    });
};