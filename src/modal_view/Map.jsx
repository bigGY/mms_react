import React from 'react';
import ReactDOM from 'react-dom';

import {
    LoadScript
} from '../common/Common';

import AMUIReact,{
    Button
} from 'amazeui-react';

import '../assets/css/manage/map.less';

class Map extends React.Component {
    constructor(props) {
        super(props);
        this.point = null;
        this.address = null;
        this.addressStr = '';
    }

    componentDidMount() {
        var dom = ReactDOM.findDOMNode(this.refs.baidumap);
        var map = new BMap.Map('baidumap');
        map.centerAndZoom("重庆",14);
        map.enableScrollWheelZoom();
        map.addControl(new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT}));
        map.addControl(new BMap.NavigationControl());
        map.addControl(new BMap.NavigationControl({anchor: BMAP_ANCHOR_TOP_RIGHT, type: BMAP_NAVIGATION_CONTROL_SMALL}));
        //逆地址获取
        var geoc = new BMap.Geocoder();
        //单击获取点击的经纬度
        map.addEventListener("click",(e)=>{
            this.point = e.point;
            map.clearOverlays();
            map.addOverlay(new BMap.Marker(e.point));             // 将标注添加到地图中
            geoc.getLocation(e.point,(rs)=>{
                this.address = rs.addressComponents;
                this.addressStr = rs.address;
            });
        });

        this._map = map;
        this._geoc = new BMap.Geocoder();
    }

    clickHandler = ()=>{
        this.props.callback({
            point:this.point,
            address:this.address,
            addressStr:this.addressStr
        });
    };

    render() {
        return (
            <div>
                <div id="baidumap" className="map_main">
                这是地图
                </div>
                <Button onClick={this.clickHandler} amStyle="primary" block>确定选中坐标</Button>
            </div>
        );
    }
}

export default Map;