import React from 'react';

import Header from '../common/Header';
import CKTable from '../components/CKTable';
import CKModal from '../components/CKModal';
import CKPages from '../components/CKPages';
import moment from 'moment';

import AMUIReact,{
    Button,
    Input,
    Form
} from 'amazeui-react';

import Fetch from '../common/Fetch';

class User extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data:null,
            currentPage:parseInt(this.props.params.id) || 1,
            dataCount:1,
            pageNum:10
        };
    }

    componentDidMount() {
        this.loadUserList(this.state.currentPage);
    }

    loadUserList(page) {
        this.refs.modal.load('正在获取数据中...');
        Fetch('/admin/account_manage/query',{query:this.getCondition(),page:page,num:this.state.pageNum},(res)=>{
            if (res.status) {
                this.setState({
                    data:res.data.data,
                    currentPage:page,
                    dataCount:res.data.count
                });
                this.refs.modal.closeModal();
            } else {
                this.refs.modal.alert('提示','没有任何数据!');
            }
        },(e)=>{
            this.refs.modal.alert('提示','获取数据出错');
        });
    }

    getCondition() {
        let query = {
            acc_username:{
                type:'like',
                name:'acc_username',
                value:this.refs.query_username.getValue()+'%'
            }
        };
        return query;
    }

    goNewUser = ()=>{
        this.context.router.push('/user_edit');
    };

    editUser = (row)=>{
        this.context.router.push('/user_edit/'+row.acc_id);
    };

    delUser = (row)=>{
        this.refs.modal.confirm('警告','你是否要删除当前用户?',()=>{
            Fetch('/admin/account_manage/del_user',{id:[row['acc_id']]},(res)=>{
                if (res.status) {
                    this.refs.modal.alert('提示','删除成功!',()=>{
                        this.context.router.push('/user');
                    });
                } else {
                    this.refs.modal.alert('错误','无法删除当前用户!');
                }
            });
        });
        console.log('删除',row);
    };

    getSelect = ()=>{
        // console.log(this.refs.table.getSelectRows());
        this.loadUserList(1);
    };

    headFormat = (value,row)=>{
        if (value) {
            return <img src={__DEBUG__?"http://localhost:8080"+value:value}
                style={{width:'30px', height:'30px', borderRadius: '15px'}}/>
        }
        return <img src={require('../assets/img/default_head.jpg')}
                    style={{width:'30px', height:'30px', borderRadius: '15px'}}/>;
    };

    render() {
        return (
            <div className="content">
                <Header title="用户管理"/>
                <div className="content_main">
                    <p>
                        <h4 className="title">查询条件</h4>
                    </p>
                    <p>
                        <Form inline>
                            <Input ref="query_username" placeholder="用户名" />
                            {'\u00a0'}
                            <Button amStyle="primary" onClick={this.getSelect}>搜索</Button>
                        </Form>
                    </p>
                    <p>
                        <Button amStyle="primary" onClick={this.goNewUser}>新增用户</Button>
                    </p>
                    <p>
                        <h4 className="title">列表</h4>
                    </p>
                    <p>
                        {this.state.data ? this.renderTable():this.renderNotData()}
                    </p>
                </div>
                <CKModal ref="modal"/>
            </div>
        );
    }

    renderTable() {
        return (
            <div>
                <CKTable ref="table" data={this.state.data} select={false} center 
                         onRefresh={()=>this.loadUserList(1)}>
                    <CKTable.Header text="ID" field="acc_id"/>
                    <CKTable.Header text="头像" field="acc_head_img" onFormat={this.headFormat}/>
                    <CKTable.Header text="登录名" field="acc_username"/>
                    <CKTable.Header text="密码" field="acc_password"/>
                    <CKTable.Header text="分组" field="grp_name"/>
                    <CKTable.Header text="真实姓名" field="acc_name"/>
                    <CKTable.Header text="EMAIL" field="acc_email"/>
                    <CKTable.Header text="创建时间" field="created_date" onFormat={(val)=>{
                        return moment.unix(val).format('YYYY-MM-DD HH:mm:ss')
                    }}/>
                    <CKTable.Header text="操作">
                        <Button onClick={this.editUser} amSize="xs" amStyle="primary">编辑</Button>
                        {'\u00a0'}
                        <Button onClick={this.delUser} amSize="xs" amStyle="danger">删除</Button>
                    </CKTable.Header>
                </CKTable>
                <CKPages current={this.state.currentPage}
                         number={this.state.pageNum}
                         count={this.state.dataCount}
                         onSelect={(page)=>{this.loadUserList(page)}}/>
            </div>
        );
    }

    renderNotData() {
        return (
            <div style={{textAlign:'center',fontSize:'24px',paddingTop:'200px'}}>
                没有数据
            </div>
        );
    }
}

User.contextTypes = {
    router: React.PropTypes.object
};

export default User;