import React from 'react';

import Header from '../common/Header';
import CKTable from '../components/CKTable';
import CKModal from '../components/CKModal';
import CKPages from '../components/CKPages';

import moment from 'moment';
import AMUIReact,{
    Button,
    Input,
    Form
} from 'amazeui-react';

import Fetch from '../common/Fetch';

class TemplateManage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            // data:[],
            page:{
                pageSize:20,
                currentPage:1,
                totalNum:43
            },
            data:null,
            currentPage:parseInt(this.props.params.id) || 1,
            dataCount:1,
            pageNum:10
        };
    }

    componentDidMount() {
        this.loadTemplate(1);
    }

    loadTemplate(page) {
        this.modal.load('正在获取数据中...');
        Fetch('/cms/template_manage/query',{query:{},page:page,num:this.state.pageNum},(res)=>{
            if (res.status) {
                this.setState({
                    data:res.data.data,
                    currentPage:page,
                    dataCount:res.data.count
                });
                this.modal.closeModal();
            } else {
                this.modal.alert('提示','没有任何数据!');
            }
        },(e)=>{
            console.log(e);
            this.modal.alert('提示','获取数据出错');
        });
    }

    addTemplate = ()=>{
        this.context.router.push('/template_edit');
    };

    editTemplate = (row)=>{
        this.context.router.push('/template_edit/'+row.cipher_id);
    };

    delTeamplate = ()=>{
        this.modal.alert('提示','暂时不能删除模板');
    };

    addFunc = (row)=>{
        this.context.router.push({
            pathname: '/func_manage/'+row.cipher_id,
            query: { name: row.tmp_name }
        });
    };

    render() {
        return (
            <div className="content">
                <Header title="模板管理"/>
                <div className="content_main">
                    <p>
                        <Button amStyle="primary" onClick={this.addTemplate}>新增模板</Button>
                    </p>
                    <p>
                        <h4 className="title">模板列表</h4>
                    </p>
                    <p>
                        {this.state.data?this.renderTable():this.renderNotData()}
                    </p>
                </div>
                <CKModal ref={(c)=>{this.modal = c}}/>
            </div>
        );
    }

    renderTable() {
        return (
            <div>
                <CKTable ref="table" data={this.state.data} select={false}
                         onRefresh={()=>this.loadTemplate(1)}>
                    <CKTable.Header text="ID" field="tmp_id"/>
                    <CKTable.Header text="操作">
                        <Button onClick={this.addFunc} amSize="xs" amStyle="success">方法添加</Button>
                    </CKTable.Header>
                    <CKTable.Header text="模板名称" field="tmp_name"/>
                    <CKTable.Header text="模板路径" field="tmp_path"/>
                    <CKTable.Header text="发布目录" field="tmp_publish_path"/>
                    <CKTable.Header text="创建时间" field="created_date" onFormat={(val)=>{
                        return moment.unix(val).format('YYYY-MM-DD HH:mm:ss')
                    }}/>
                    <CKTable.Header text="操作">
                        <Button onClick={this.editTemplate} amSize="xs" amStyle="primary">编辑</Button>
                        {'\u00a0'}
                        <Button onClick={this.delTeamplate} amSize="xs" amStyle="danger">删除</Button>
                    </CKTable.Header>
                </CKTable>
                <CKPages current={this.state.currentPage}
                         number={this.state.pageNum}
                         count={this.state.dataCount}
                         onSelect={(page)=>{this.loadTemplate(page)}}/>
            </div>
        );
    }

    renderNotData() {
        return (
            <div style={{textAlign:'center',fontSize:'24px',paddingTop:'200px'}}>
                没有数据
            </div>
        );
    }
}

TemplateManage.contextTypes = {
    router: React.PropTypes.object
};

export default TemplateManage;