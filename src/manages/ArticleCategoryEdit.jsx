import React from 'react';

import Header from '../common/Header';
import CKModal from '../components/CKModal';
import Validate from '../common/Validate';
import moment from 'moment';
import AMUIReact,{
    Button,
    Input,
    Form,
    Icon,
    Selected,
    FormGroup
} from 'amazeui-react';

import Fetch from '../common/Fetch';

class ArticleCategoryEdit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data:{}
        };
    }

    componentDidMount() {

    }

    changeHandler() {
        return ()=>{

        }
    }

    render() {
        let data = this.state.data;
        return (
            <div className="content">
                <Header title={this.state.type === 'edit'?'修改文章分类':'添加文章分类'} back={true}/>
                <div className="content_main">
                    <div style={{marginBottom:'20px'}}>
                        <h4 className="title">模板信息</h4>
                    </div>
                    <Form ref="from" horizontal style={{marginTop:'20px'}}>
                        <Input id="menu_name" ref="menu_name" label="模板名称"
                               labelClassName="am-u-sm-2"
                               wrapperClassName="am-u-sm-4 am-u-end"
                               value={data.tmp_name}
                               placeholder="模板名称为2到40个字符"
                               onChange={this.changeHandler('tmp_name')}
                        />
                        <Input id="menu_link" ref="menu_link" label="模板路径"
                               labelClassName="am-u-sm-2"
                               placeholder="模板方法为2到40个任意字符"
                               wrapperClassName="am-u-sm-4 am-u-end"
                               value={data.tmp_path}
                               onChange={this.changeHandler('tmp_path')}
                        />
                        <Input id="menu_icon" ref="menu_icon" label="发布目录"
                               labelClassName="am-u-sm-2"
                               wrapperClassName="am-u-sm-4 am-u-end"
                               value={data.tmp_publish_path}
                               onChange={this.changeHandler('tmp_publish_path')}
                        />
                        <Input label="添加时间" labelClassName="am-u-sm-2"
                               wrapperClassName="am-u-sm-4 am-u-end"
                               value={moment.unix(data.created_date).format('YYYY-MM-DD HH:mm:ss')}
                               disabled
                        />
                    </Form>
                    <div className="am-form-group">
                        <div className="am-u-sm-2">
                            &nbsp;
                        </div>
                        <div className="am-u-sm-4 am-u-end">
                            <Button amStyle="primary"
                                    onClick={this.save}
                                    disabled={this.state.is_save}
                            >
                                {this.state.is_save?'保存中...':'保存'}
                            </Button>
                        </div>
                    </div>
                </div>
                <CKModal ref={(c)=>{this.modal = c}}/>
            </div>
        )
    }
}

export default ArticleCategoryEdit;