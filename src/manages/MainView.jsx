import React from 'react';

import Header from '../common/Header';

import Fetch from '../common/Fetch';

class MainView extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        return (
            <div className="content">
                <Header/>
                <div className="content_main">
                    <p>
                        这是首屏页面
                    </p>
                </div>
            </div>
        );
    }
}

MainView.contextTypes = {
    router: React.PropTypes.object
};

export default MainView;