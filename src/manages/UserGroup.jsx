import React from 'react';

import Header from '../common/Header';
import CKTable from '../components/CKTable';
import CKModal from '../components/CKModal';
import CKPages from '../components/CKPages';
import moment from 'moment';
import AMUIReact,{
    Button,
    Input,
    Form
} from 'amazeui-react';

import Fetch from '../common/Fetch';

class UserGroup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data:null,
            currentPage:parseInt(this.props.params.id) || 1,
            dataCount:1,
            pageNum:10
        };
    }

    componentDidMount() {
        this.loadUserList(this.state.currentPage);
    }

    loadUserList(page) {
        this.refs.modal.load('正在获取数据中...');
        Fetch('/admin/group_manage/get_all',{page:page,num:this.state.pageNum},(res)=>{
            if (res.status) {
                this.setState({
                    data:res.data.data,
                    currentPage:page,
                    dataCount:res.data.count
                });
                this.refs.modal.closeModal();
            } else {
                this.refs.modal.alert('提示','没有任何数据!');
            }
        },(e)=>{
            console.log(e);
            this.refs.modal.alert('提示','获取数据出错');
        });
    }

    addGroup = ()=>{
        this.context.router.push('/group_edit');
    };

    editGroup = (row)=>{
        this.context.router.push('/group_edit/'+row.cipher_id);
    };

    delGroup = (row)=>{
        this.refs.modal.confirm('警告','你是否要删除当前分组?',()=> {
            Fetch('/admin/group_manage/delete', {id: row.grp_id}, (res)=> {
                if (res.status) {
                    if(res.data.msg){
                        this.refs.modal.alert('提示','删除成功!',()=>{
                            this.context.router.push('/user_group');
                        });
                    }else{
                        this.refs.modal.alert('提示', res.data.tip);
                    }
                } else {
                    this.refs.modal.alert('提示', '删除失败');
                }
            }, (e)=> {
                this.refs.modal.alert('提示', '请求错误');
            });
        });
    };

    render() {
        return (
            <div className="content">
                <Header title="分组管理"/>
                <div className="content_main">
                    <p>
                        <Button amStyle="primary" onClick={this.addGroup}>新增分组</Button>
                    </p>
                    <p>
                        <h4 className="title">列表</h4>
                    </p>
                    <p>
                        {this.state.data?this.renderTable():this.renderNotData()}
                    </p>
                </div>
                <CKModal ref="modal"/>
            </div>
        );
    }

    renderTable() {
        return (
            <div>
            <CKTable ref="table" data={this.state.data} select={false}
                     onRefresh={()=>this.loadUserList(1)}>
                <CKTable.Header text="ID" field="grp_id"/>
                <CKTable.Header text="分组名称" field="grp_name"/>
                <CKTable.Header text="创建时间" field="created_date" onFormat={(val)=>{
                    return moment.unix(val).format('YYYY-MM-DD HH:mm:ss')
                }}/>
                <CKTable.Header text="修改时间" field="modified_date" onFormat={(val)=>{
                    return moment.unix(val).format('YYYY-MM-DD HH:mm:ss')
                }}/>
                <CKTable.Header text="操作">
                    <Button onClick={this.editGroup} amSize="xs" amStyle="primary">编辑</Button>
                    {'\u00a0'}
                    <Button onClick={this.delGroup} amSize="xs" amStyle="danger">删除</Button>
                </CKTable.Header>
            </CKTable>
            <CKPages current={this.state.currentPage}
                     number={this.state.pageNum}
                     count={this.state.dataCount}
                     onSelect={(page)=>{this.loadUserList(page)}}/>
            </div>
        );
    }

    renderNotData() {
        return (
            <div style={{textAlign:'center',fontSize:'24px',paddingTop:'200px'}}>
                没有数据
            </div>
        );
    }
}

UserGroup.contextTypes = {
    router: React.PropTypes.object
};

export default UserGroup;