import React from 'react';

import Header from '../common/Header';
import CKModal from '../components/CKModal';
import Validate from '../common/Validate';
import moment from 'moment';
import AMUIReact,{
    Button,
    Input,
    Form,
    Icon,
    Selected,
    FormGroup
} from 'amazeui-react';

import Fetch from '../common/Fetch';

import $ from 'jquery';

class TemplateEdit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data:{
                tmp_id:null,
                tmp_name:'',
                tmp_path:'',
                tmp_publish_path:'',
                created_date:''
            },
            is_save:false,
            type:this.props.params.id ? 'edit':'add',
            init:false,
        };
    }

    componentDidMount() {
        if (this.state.type == 'edit') {
            this.loadTemplateData();
        }
    }

    loadTemplateData() {
        Fetch('/cms/template_manage/info',{cipher_id:this.props.params.id},(res)=>{
            if (res.status) {
                let data = this.state.data;
                $.map(data,(item,index)=>{
                     data[index] = res.data[index];
                });
                this.setState({
                    data:data
                });
            } else {
                this.modal.alert('错误','获取模板信息出错！');
            }
        });
    }

    changeHandler(field){
        return (e)=>{
            let info = this.state.data;
            info[field] = e.target.value;
            this.setState({data:info});
        }
    }

    checkData() {
        let valid = {
            'tmp_name':{role:'s2-20',errmsg:'模板名称为2到20个字符'},
            'tmp_path':{role:'*',errmsg:'模板路径不能为空'}
        };

        let data = this.state.data;

        try {
            Validate.inst().checkData(data,valid);
            return data;
        } catch (e) {
            this.modal.alert('错误',e);
            return false;
        }
    }

    save = ()=>{
        let data = this.checkData();
        if (data === false) return;

        if (this.state.is_save) return;
        this.setState({
            is_save:true
        });

        Fetch('/cms/template_manage/save',{data:data},(res)=>{
            if (res.status) {
                this.modal.alert('成功','保存菜单数据成功!',()=>{
                    this.context.router.push('/template_manage');
                });
            } else {
                this.modal.alert('失败','保存菜单失败!!');
                this.setState({
                    is_save:false
                });
            }
        },()=>{
            this.modal.alert('失败','远程调用失败!!');
            this.setState({
                is_save:false
            });
        });
    };

    render() {
        let data = this.state.data;
        return (
            <div className="content">
                <Header title={this.state.type === 'edit'?'修改模板':'添加模板'} back={true}/>
                <div className="content_main">
                    <div style={{marginBottom:'20px'}}>
                        <h4 className="title">模板信息</h4>
                    </div>
                    <Form ref="from" horizontal style={{marginTop:'20px'}}>
                        <Input id="menu_name" ref="menu_name" label="模板名称"
                               labelClassName="am-u-sm-2"
                               wrapperClassName="am-u-sm-4 am-u-end"
                               value={data.tmp_name}
                               placeholder="模板名称为2到40个字符"
                               onChange={this.changeHandler('tmp_name')}
                        />
                        <Input id="menu_link" ref="menu_link" label="模板路径"
                               labelClassName="am-u-sm-2"
                               placeholder="模板方法为2到40个任意字符"
                               wrapperClassName="am-u-sm-4 am-u-end"
                               value={data.tmp_path}
                               onChange={this.changeHandler('tmp_path')}
                        />
                        <Input id="menu_icon" ref="menu_icon" label="发布目录"
                               labelClassName="am-u-sm-2"
                               wrapperClassName="am-u-sm-4 am-u-end"
                               value={data.tmp_publish_path}
                               onChange={this.changeHandler('tmp_publish_path')}
                        />
                        <Input label="添加时间" labelClassName="am-u-sm-2"
                               wrapperClassName="am-u-sm-4 am-u-end"
                               value={moment.unix(data.created_date).format('YYYY-MM-DD HH:mm:ss')}
                               disabled
                        />
                    </Form>
                    <div className="am-form-group">
                        <div className="am-u-sm-2">
                            &nbsp;
                        </div>
                        <div className="am-u-sm-4 am-u-end">
                            <Button amStyle="primary"
                                    onClick={this.save}
                                    disabled={this.state.is_save}
                            >
                                {this.state.is_save?'保存中...':'保存'}
                            </Button>
                        </div>
                    </div>
                </div>
                <CKModal ref={(c)=>{this.modal = c}}/>
            </div>
        );
    }
}

TemplateEdit.contextTypes = {
    router: React.PropTypes.object
};

export default TemplateEdit;