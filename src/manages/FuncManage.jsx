import React from 'react';

import Header from '../common/Header';
import CKTable from '../components/CKTable';
import CKModal from '../components/CKModal';
import CKPages from '../components/CKPages';

import moment from 'moment';
import AMUIReact,{
    Button,
    Input,
    Form
} from 'amazeui-react';

import Fetch from '../common/Fetch';

class FuncManage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // data:[],
            page:{
                pageSize:20,
                currentPage:1,
                totalNum:43
            },
            data:null,
            dataCount:1,
            pageNum:10
        };
        this.tmp_id = this.props.params.id;
    }

    componentDidMount() {
        this.loadFunctions(1);
    }

    loadFunctions(page) {
        this.modal.load('正在获取数据中...');
        Fetch('/cms/func_manage/query',{tmp_id:this.tmp_id,query:this.condition(),page:page,num:this.state.pageNum},(res)=>{
            if (res.status) {
                this.setState({
                    data:res.data.data,
                    currentPage:page,
                    dataCount:res.data.count
                });
                this.modal.closeModal();
            } else {
                this.modal.alert('提示','没有任何数据!');
            }
        },(e)=>{
            console.log(e);
            this.modal.alert('提示','获取数据出错');
        });
    }

    condition() {
        return {};
    }

    addFunc = ()=>{
        this.context.router.push({
            pathname:'/func_edit',
            query:{
                name:this.props.location.query.name,
                tmp_id:this.props.params.id
            }
        });
    };

    editFunc = (row)=>{
        this.context.router.push({
            pathname:'/func_edit/'+row.cipher_id,
            query:{
                name:this.props.location.query.name,
                tmp_id:this.props.params.id
            }
        });
    };

    render() {
        return (
            <div className="content">
                <Header title={"模板方法管理 - "+this.props.location.query.name} back/>
                <div className="content_main">
                    <p>
                        <Button amStyle="primary" onClick={this.addFunc}>新增方法</Button>
                    </p>
                    <p>
                        <h4 className="title">方法列表</h4>
                    </p>
                    <p>
                        {this.state.data?this.renderTable():this.renderNotData()}
                    </p>
                </div>
                <CKModal ref={(c)=>{this.modal = c}}/>
            </div>
        );
    }

    renderTable() {
        return (
            <div>
                <CKTable ref="table" data={this.state.data} select={false}
                         onRefresh={()=>this.loadFunctions(1)}>
                    <CKTable.Header text="ID" field="fun_id"/>
                    <CKTable.Header text="方法名称" field="fun_name"/>
                    <CKTable.Header text="方法变量" field="fun_var"/>
                    <CKTable.Header text="函数类" field="fun_class"/>
                    <CKTable.Header text="函数方法" field="fun_function"/>
                    <CKTable.Header text="创建时间" field="created_date" onFormat={(val)=>{
                        return moment.unix(val).format('YYYY-MM-DD HH:mm:ss')
                    }}/>
                    <CKTable.Header text="操作">
                        <Button onClick={this.editFunc} amSize="xs" amStyle="primary">编辑</Button>
                        {'\u00a0'}
                        <Button onClick={this.delFunc} amSize="xs" amStyle="danger">删除</Button>
                    </CKTable.Header>
                </CKTable>
                <CKPages current={this.state.currentPage}
                         number={this.state.pageNum}
                         count={this.state.dataCount}
                         onSelect={(page)=>{this.loadFunctions(page)}}/>
            </div>
        );
    }

    renderNotData() {
        return (
            <div style={{textAlign:'center',fontSize:'24px',paddingTop:'200px'}}>
                没有数据
            </div>
        );
    }
}

FuncManage.contextTypes = {
    router: React.PropTypes.object
};

export default FuncManage;