import React from 'react';

import Header from '../common/Header';
import CKTable from '../components/CKTable';
import CKModal from '../components/CKModal';
import CKPages from '../components/CKPages';

import moment from 'moment';
import {
    Button,
    Input,
    Form
} from 'amazeui-react';

import Fetch from '../common/Fetch';

class ArticleCategoryManage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            page:{
                pageSize:20,
                currentPage:1,
                totalNum:43
            },
            data:null,
            currentPage:parseInt(this.props.params.id) || 1,
            dataCount:1,
            pageNum:10
        };
    }

    componentDidMount() {
        this.loadCategory(1);
    }

    addCategory = ()=>{
        this.context.router.push('/article_category_edit');
    };

    editCategory = (row)=>{
        this.context.router.push('/article_category_edit/'+row.cipher_id);
    };

    loadCategory(page) {
        this.modal.load('正在获取数据中...');
        Fetch('/cms/article_category_manage/query',{query:{},num:this.state.pageNum,page:page},(res)=>{
            if (res.status) {
                this.setState({
                    data:res.data.data,
                    currentPage:page,
                    dataCount:res.data.count
                });

                this.modal.closeModal();
            } else {
                this.modal.alert('失败','调用文章分类列表出错！'+res.msg);
            }
        });
    }

    render() {
        return (
            <div className="content">
                <Header title="文章分类管理"/>
                <div className="content_main">
                    <p>
                        <Button amStyle="primary" onClick={this.addCategory}>新增分类</Button>
                    </p>
                    <p>
                        <h4 className="title">文章分类列表</h4>
                    </p>
                    <p>
                        {this.state.data?this.renderTable():this.renderNotData()}
                    </p>
                </div>
                <CKModal ref={(c)=>{this.modal = c}}/>
            </div>
        );
    }

    renderTable() {
        return (
            <div>
                <CKTable ref="table" data={this.state.data} select={false}
                         onRefresh={()=>this.loadCategory(1)}>
                    <CKTable.Header text="ID" field="ctg_id"/>
                    <CKTable.Header text="分类名称" field="ctg_name"/>
                    <CKTable.Header text="发布目录" field="ctg_publish_path"/>
                    <CKTable.Header text="模板名称" field="tmp_name"/>
                    <CKTable.Header text="创建时间" field="created_date" onFormat={(val)=>{
                        return moment.unix(val).format('YYYY-MM-DD HH:mm:ss')
                    }}/>
                    <CKTable.Header text="操作">
                        <Button onClick={this.editCategory} amSize="xs" amStyle="primary">编辑</Button>
                        {'\u00a0'}
                        <Button onClick={this.delTeamplate} amSize="xs" amStyle="danger">删除</Button>
                    </CKTable.Header>
                </CKTable>
                <CKPages current={this.state.currentPage}
                         number={this.state.pageNum}
                         count={this.state.dataCount}
                         onSelect={(page)=>{this.loadCategory(page)}}/>
            </div>
        );
    }

    renderNotData() {
        return (
            <div style={{textAlign:'center',fontSize:'24px',paddingTop:'200px'}}>
                没有数据
            </div>
        );
    }
}

ArticleCategoryManage.contextTypes = {
    router: React.PropTypes.object
};

export default ArticleCategoryManage;