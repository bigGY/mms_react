import React from 'react';
import {LoadScript} from '../common/Common';

import '../asset/css/color.less';

class CKColor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
        
        this.callback = null;
    }

    componentDidMount() {
        LoadScript.load('/vendor/colorpicker/css/bootstrap-colorpicker.min.css','color_css',()=>{
            LoadScript.load('/vendor/colorpicker/js/bootstrap-colorpicker.min.js','color_js',()=>{
                this.initPicker();
            });
        });
    }

    componentWillUnmount() {
        this.close();
    }

    initPicker() {
        if ($('#color_picker').length === 0) {
            $('<div id="color_picker" class="color_picker"/>').appendTo(document.body);
        }
        $('#color_picker').colorpicker({
            color: '#ffaa00',
            container: true,
            inline: true,
            customClass:'color_picker_dia',
            format:'hex'
        }).on('changeColor',this.changeHandler);
    }

    changeHandler = (e)=>{
        if (this.callback) {
            this.callback(e.color.toHex().toLocaleUpperCase());
        }
    };

    show(point,value,callback) {
        this.callback = callback;
        $('#color_picker').css({
            top:point.top,
            left:point.left
        }).show(100).colorpicker('setValue', value);
    }
    
    close() {
        this.callback = null;
        $('#color_picker').hide(100);
    }

    render() {
        return (
            <div className="color_picker">

            </div>
        );
    }
}

export default CKColor;