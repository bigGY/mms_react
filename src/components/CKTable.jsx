import React from 'react';

import {CKCheckbox,CKHeader} from './CKHeader';

import AMUIReact,{
    Table,
    Button,
    Icon
} from 'amazeui-react';

class CKTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data:this.props.data,
            select:this.props.select,
            refresh:this.props.onRefresh ? true:false
        };

        this.select_all = false;

        this.selectRows = {};
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.data !== nextProps.data) {
            this.setState({
                data:nextProps.data
            });
        }
    }

    changeHandler(row,i) {
        return (checked)=>{
            if (checked) {
                this.selectRows[i] = row;
            } else {
                this.selectRows[i] = undefined;
            }
        };
    }

    selectAll = (checked)=>{
        this.select_all = checked;
        $.map(this.refs,(item)=>{
            item.check(this.select_all);
        });
    };

    /**
     * 得到所有选中的行
     * @returns {*}
     */
    getSelectRows() {
        return $.map(this.selectRows,(item)=>{
            return item;
        });
    }

    render() {
        return (
            <div>
                {this.state.refresh ? (<Button onClick={this.props.onRefresh} amSize="sm">
                    <Icon icon="refresh" /> 刷新列表
                </Button>) : null}
                <Table striped hover radius className={this.props.center?'am-table-centered':''}>
                    {this.props.header?this.renderHeader():null}
                    <tbody>
                    {this.state.data.map((row,i)=>{
                        return this.renderRow(row,i);
                    })}
                    </tbody>
                </Table>
            </div>
        );
    }

    renderHeader() {
        return (
            <thead>
                <tr>
                    {this.state.select ? <th><CKCheckbox type="checkbox" onChange={this.selectAll}/></th> : null}
                    {React.Children.map(this.props.children,(item,key)=>{
                        return (
                            <th key={'head_'+key}>{item.props.text}</th>
                        );
                    })}
                </tr>
            </thead>
        );
    }

    renderRow(row,i) {
        return (
            <tr>
                {this.state.select ? <th><CKCheckbox ref={'row_'+i} type="checkbox" onChange={this.changeHandler(row,i)}/></th> : null}
                {React.Children.map(this.props.children,(item,key)=>{
                    if (item.props.children) {
                        return (
                            <td key={'col_'+key}>{React.cloneElement(item,{text:item.props.text,row:row,value:row[item.props.field]})}</td>
                        );
                    } else {
                        return <td key={'col_'+key}>{item.props.onFormat?item.props.onFormat(row[item.props.field]):row[item.props.field]}</td>;
                    }
                })}
            </tr>
        );
    }
}

CKTable.propTypes = {
    select:React.PropTypes.bool,
    header:React.PropTypes.bool,
    center:React.PropTypes.bool
};

CKTable.defaultProps = {
    data:[],
    select:true,
    header:true,
    center:false,
    onRefresh:null,
    pages:{
        currentPage:1,
        pageCount:1
    }
};

CKTable.Header = CKHeader;

export default CKTable;