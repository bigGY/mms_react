import React from 'react';

import AMUIReact,{
    Modal,
    ModalTrigger
} from 'amazeui-react';

import loader from '../common/Loader';
import NotFound from './manage/NotFound';
import '../assets/css/modal.less';

class CKModal extends React.Component {
    constructor(props) {
        super(props);

        this._classList = {
            '800':'modal_800'
        };

        this.state = {
            title:this.props.title,
            content:this.props.content,
            showModal:false,
            type:'alert',
            closeIcon:true,
            className:''
        };
        
        this.events = {
            onClose:this.props.onClose,
            onConfirm:this.props.onConfirm,
            onCancel:this.props.onCancel
        };
    }

    ucfirst(str) {
        var first = str[0].toUpperCase();
        return first+str.substr(1);
    }

    under2hump(str) {
        var arr = str.split('_');
        var hump = arr.map((item)=>{
            return this.ucfirst(item);
        });
        return hump.join('');
    }

    alert(title,content,fn) {
        this.events.onConfirm = fn || this.props.onConfirm;
        
        this.setState({
            showModal:true,
            type:'alert',
            title:title,
            content:content
        });
    }

    load(content) {
        this.events.onClose = this.props.onClose;
        this.setState({
            showModal:true,
            type:'loading',
            title:content,
            content:null
        });
    }

    confirm(title,content,ok,cancel) {
        this.events.onCancel = cancel || this.props.onCancel;
        this.events.onConfirm = ok || this.props.onConfirm;
        this.setState({
            showModal:true,
            type:'confirm',
            title:title,
            content:content
        });
    }

    view(title, viewName, data, fn, size) {
        this.events.onCallback = fn;
        
        this.setState({
            showModal:true,
            type:'popup',
            title:title,
            content:this.getView(viewName,data),
            className:this._classList[size] || ''
        });
    }
    
    getView(viewName,data) {
        let router_name = this.under2hump(viewName);
        try {
            let component = loader(require('bundle?lazy!babel?presets=react!../modal_view/'+router_name+'.jsx'));
            return React.createElement(component,{parent:this,params:data,callback:this.callbackHandler});
        } catch(e) {
            return <NotFound component={router_name}/>;
        }
    }

    closeModal() {
        this.setState({
            showModal:false
        });
        this.refs.modal.close();
    }

    closeHandler = ()=>{
        this.setState({
            showModal:false
        });
        this.events.onClose();
    };

    callbackHandler = (val)=>{
        if (this.events.onCallback) {
            this.events.onCallback(val);
        }
        this.closeModal();
    };

    cancelHandler = ()=>{
        this.setState({
            showModal:false
        });
        this.events.onCancel();
    };

    confirmHandler = ()=>{
        this.setState({
            showModal:false
        });
        this.events.onConfirm();
    };

    render() {
        var modalTrigger,modal;
        modal = (
            <Modal type={this.state.type} title={this.state.title} className={'madal_fix '+this.state.className}>
                {this.state.content}
            </Modal>
        );
        if (this.state.type === 'alert' || this.state.type === 'loading') {
            modalTrigger = (
                <ModalTrigger
                    ref="modal"
                    modal={modal}
                    show={this.state.showModal}
                    onConfirm={this.confirmHandler}
                />
            );
        } else if (this.state.type === 'confirm') {
            modalTrigger = (
                <ModalTrigger
                    ref="modal"
                    modal={modal}
                    show={this.state.showModal}
                    onCancel={this.cancelHandler}
                    onConfirm={this.confirmHandler}
                />
            );
        } else if (this.state.type === 'popup') {
            modalTrigger = (
                <ModalTrigger
                    ref="modal"
                    modal={modal}
                    show={this.state.showModal}
                    onClose={this.closeHandler}
                />
            );
        }

        return modalTrigger;
    }
}

CKModal.defaultProps = {
    onClose:function(){},
    onConfirm:function(){},
    onCancel:function () {}
};

export default CKModal;