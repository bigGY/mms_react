import React from 'react';

import loader from '../../common/Loader';

import NotFound from './NotFound';

class Detail extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    ucfirst(str) {
        var first = str[0].toUpperCase();
        return first+str.substr(1);
    }

    under2hump(str) {
        var arr = str.split('_');
        var hump = arr.map((item)=>{
            return this.ucfirst(item);
        });
        return hump.join('');
    }

    render() {
        let router_name = this.under2hump(this.props.params.component);
        try {
            let component = loader(require('bundle?lazy!babel?presets=react!../../manages/'+router_name+'.jsx'));
            return React.createElement(component,this.props);
        } catch(e) {
            return <NotFound component={router_name}/>;
        }
    }
}

export default Detail;
