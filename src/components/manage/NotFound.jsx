import React from 'react';

class NotFound extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={{textAlign:'center',fontSize:'24px',paddingTop:'200px'}}>
                404，没找到相应的模块 <span style={{color:'red'}}>{this.props.component}</span>
            </div>
        );
    }
}

export default NotFound;
