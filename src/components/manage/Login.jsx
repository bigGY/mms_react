import React from 'react';

import AMUIReact,{
    Button,
    Input,
    Form,
    Icon
} from 'amazeui-react';

import CKModal from '../../components/CKModal';

import '../../assets/css/manage/login.less';

import Fetch from '../../common/Fetch';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            init:false
        };
        this.user = null;
    }

    componentDidMount() {
        document.title = 'ZCXF Main Management System';
        window.CKLogin = this;
        this.checkLogin();
    }

    getUser() {
        return this.user;
    }

    logout() {
        Fetch('/admin/login/logout',{},(res)=>{
            if (res.status) {
                this.user = null;
                this.props.parent.setLogin(false);
                this.context.router.push('/');
            }
        });
    }

    checkLogin() {
        Fetch('/admin/login/auth',{},(res)=>{
            if (res.status) {
                this.user = res.data;
                this.props.parent.setLogin(true);
            } else {
                this.props.parent.setLogin(false);
                this.setState({
                    init:true
                });
            }
        });
    }

    onLogin = (e)=>{
        this._modal.load("正在登录中...");
        Fetch('/admin/login/admin_login',{
            username:this.refs.username.getValue(),
            password:this.refs.passwd.getValue()
        },(res)=>{
            if (res.status) {
                this.user = res.data;
                this.props.parent.setLogin(true);
            } else {
                this._modal.alert('错误','用户名或密码错误,请重试');
            }
        });
    };

    keyboardHandler = (e)=>{
        if (e.keyCode === 13) {
            this.onLogin(e);
        }
    };

    render() {
        if (!this.state.init) {
            return this.renderLoad();
        }

        return (
            <div className="login_main">
                <div className="login_window">
                    <p style={{marginBottom:'20px'}}>
                        <h2>众创 MMS 系统</h2>
                    </p>
                    <p>
                        <Input ref="username" placeholder="用户名" onKeyDown={this.keyboardHandler}/>
                    </p>
                    <p>
                        <Input ref="passwd" type="password" placeholder="密码" onKeyDown={this.keyboardHandler}/>
                    </p>
                    <p style={{marginTop:'10px'}}>
                        <Button amStyle="primary" block onClick={this.onLogin}>登录</Button>
                    </p>
                </div>
                <CKModal ref={(c)=>this._modal = c}/>
            </div>
        )
    }

    renderLoad() {
        return (
            <div className="login_main">
                <div className="login_window">
                    加载登录中...
                </div>
            </div>
        );
    }
}

Login.contextTypes = {
    router: React.PropTypes.object
};

export default Login;