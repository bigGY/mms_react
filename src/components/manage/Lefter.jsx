import React from 'react';

import TreeMenu from './TreeMenu';
import CKModal from '../CKModal';

import AMUIReact,{
    Button,
    Selected,
    Grid,
    Col,
    Dropdown
} from 'amazeui-react';

import Fetch from '../../common/Fetch';

import '../../assets/css/manage/dropdown_menu.less';

class Lefter extends React.Component {
    constructor(props) {
        super(props);
        var data = [
            {
                menu_id:1,
                menu_text:'系统管理',
                sub_menus:[
                    {menu_id:2,menu_text:'后台用户管理',menu_link:'/user'},
                    {menu_id:3,menu_text:'后台组管理',menu_link:'/user_group'},
                    {menu_id:4,menu_text:'后台菜单管理',menu_link:'/menu_manage'},
                    {menu_id:5,menu_text:'站点列表',menu_link:'/site_manage'},
                    {menu_id:7,menu_text:'商户管理',menu_link:'/merchant_manage'}
                ]
            }
        ];
        var user = CKLogin.getUser();
        // var user = {};
        this.state = {
            data:data,
            head:user.acc_head_img || require('../../assets/img/default_head.jpg'),
            user:user,
            init_menu:false
        };
    }

    componentDidMount() {
        this.loadUserMenu();
    }

    loadUserMenu() {
        Fetch('/admin/group_manage/get_user_pur_menu',{},(res)=>{
            if (res.status) {
                this.setState({
                    data:res.data
                });
            } else{
                this._modal.alert('获取权限数据出错!!!');
            }
        },(e)=>{
            console.log(e);
            this._modal.alert('远程调用出错!');
        });
    }

    closeHandler = (value)=>{
        CKLogin.logout();
    };

    render() {
        return (
            <div className="lefter">
                <div className="user_info">
                    <Grid className="doc-g">
                        <Col sm={6}>
                            <img className="user" src={this.state.head}/>
                        </Col>
                        <Col sm={6}>
                            <Dropdown className="user_btn"
                                      title={this.state.user.acc_name}
                                      btnStyle="primary"
                                      btnSize="sm">
                                <ul className="drop_menu_list">
                                    <li onClick={this.closeHandler}>退出登录</li>
                                </ul>
                            </Dropdown>
                        </Col>
                    </Grid>
                </div>
                <TreeMenu data={this.state.data}/>
                <CKModal ref={(c) => this._modal = c}/>
            </div>
        );
    }
}

export default Lefter;