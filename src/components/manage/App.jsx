import React from 'react';

import Lefter from './Lefter';

import Login from './Login';

import {
    Container
} from 'amazeui-react';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            is_login:false
        };
    }
    
    componentDidMount() {

    }

    setLogin(flag) {
        this.setState({
            is_login: flag
        });
    }

    render() {
        if (!this.state.is_login) {
            return (<Login parent={this}/>);
        }
        return (
            <Container className="main">
                <Lefter/>
                {this.props.children}
            </Container>
        );
    }
}

export default App;