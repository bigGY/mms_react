import React from 'react';
import {
    Router,
    Route,
    hashHistory,
    useRouterHistory,
    IndexRoute,
} from 'react-router';

import App from './components/manage/App';
import Detail from './components/manage/Detail';
import MainView from './manages/MainView';

import createHashHistory from 'history/lib/createHashHistory';
const history = useRouterHistory(createHashHistory)({ queryKey: false });

/* global SERVER_RENDING */
const routes = (
    <Router history={history}>
        <Route path="/" component={App}>
            <Route path=":component(/:id)" component={Detail} />
            <IndexRoute component={MainView} />
        </Route>
    </Router>
);

export default routes;