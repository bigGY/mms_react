/**
 * 主程序入口
 **/
import React from 'react';

import {render} from 'react-dom';

import './assets/css/manage/main.less';

import routers from './manage_routers';

render(routers,document.getElementById('react-main')) ;