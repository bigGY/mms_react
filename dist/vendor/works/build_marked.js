/**
 * Created by CLAKE on 2016/11/23.
 */
importScripts('http://cdn.bootcss.com/marked/0.3.6/marked.min.js');
importScripts('../code/highlight.pack.js');
onmessage = function (oEvent) {
    var render = new marked.Renderer();
    render.code = function(code,lang){
        var code_html = '<pre><code class="hljs '+lang+'">';
        code_html += hljs.highlight(lang,code,true).value;

        code_html += '</code></pre>';

        return code_html;
    };
    marked.setOptions({
        renderer: render,
        gfm: true,
        tables: true,
        breaks: true,
        pedantic: false,
        sanitize: true,
        smartLists: true,
        smartypants: false
    });
    marked.setOptions({
        highlight: function (code, lang, callback) {
            console.log(1);
            return hljs.highlight(lang,code,true).value;
        }
    });
    var html = marked(oEvent.data);
    postMessage(html);
};