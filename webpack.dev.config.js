/**
 * Created by CLAKE on 2016/8/9.
 */
var ip = require('ip');
const ip_address = ip.address();
var webpackConfig = require('./webpack.config');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
webpackConfig.entry = {
    // app: [
    //     //资源服务器地址
    //     'webpack/hot/dev-server',
    //     'webpack-hot-middleware/client?reload=true',
    //     './src/app.jsx'
    // ],
    manage: [
        //资源服务器地址
        'webpack/hot/dev-server',
        'webpack-hot-middleware/client?reload=true',
        './src/manage.jsx'
    ]
};

webpackConfig.plugins = [
    new ExtractTextPlugin('[name].css'),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.CommonsChunkPlugin('common.js'),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
        "process.env": {
            NODE_ENV: JSON.stringify("development")
        }
    })
];

// webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin());
// webpackConfig.plugins.push(
//     new webpack.DefinePlugin({
//         'process.env.NODE_ENV': '"development"'
//     })
// );

//webpackConfig.devServer = {
//    historyApiFallback: true,
//    hot: true,
//    inline: true,
//    progress: true,
//    colors:true
//};

webpackConfig.devtool = 'eval-source-map';

export default webpackConfig;