/**
 * Created by CLAKE on 2016/8/9.
 */
var process = require('process');
var ENV = process.env.NODE_ENV;
var ip = require('ip');
var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var commonsPlugin = new webpack.optimize.CommonsChunkPlugin('common.js');
var processPlugin = new webpack.DefinePlugin({
    "process.env": {
        NODE_ENV: JSON.stringify("production")
    }
});
var extractPlugin = new ExtractTextPlugin("[name].css");

var node_modules = path.resolve(__dirname, 'node_modules');
var react = path.resolve(node_modules, 'react/dict/react.js');

const ip_address = ip.address();

module.exports = {
    //插件项
    plugins: [
        processPlugin,
        commonsPlugin,
        new ExtractTextPlugin("[name].css"),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ],
    //页面入口文件配置
    entry: {
    //     //主文件
    //     app : './src/app.jsx',
        //管理中心
        manage : './src/manage.jsx'
    },
    output: {
        path: `${__dirname}/dist`,
        filename: '[name].js',
        chunkFilename:`./manage/chunk/[name].[chunkhash:8].js`
    },
    module: {
        //加载器配置
        loaders: [
            {
                test: /\.css$/,
                loader: ENV == 'production' ? ExtractTextPlugin.extract("style-loader","css-loader") : 'style!css'
            },
            {
                test: /\.less$/,
                loader: ENV == 'production' ? ExtractTextPlugin.extract("style-loader","css-loader!less-loader") : 'style!css!less'
            },
            { test: /\.woff[2]?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
            { test: /\.ttf$/,  loader: "url-loader?limit=10000&mimetype=application/octet-stream" },
            { test: /\.eot$/,  loader: "file-loader" },
            { test: /\.svg$/,  loader: "url-loader?limit=10000&mimetype=image/svg+xml" },
            {
                test: /\.jsx$/,
                loader: 'babel',
                exclude: /node_modules/,
                query:{
                    presets:['es2015', 'stage-0', 'react']
                }
            },
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/
            },
            {
                test: /\.(jpe?g|png|gif)$/i,
                loader: 'url?limit=10000&name=img/[hash:8].[name].[ext]'
            },
            {
                test: /\.jsx$/,
                loader: 'string-replace',
                query: {
                    search: '$$DEBUG$$',
                    replace: 'false'
                }
            },
        ],
        noParse:[react]
    },
    //其它解决方案配置
    resolve: {
        extensions: ['', '.js', '.json', '.less', '.jsx']
        // ,alias: {
        //     AppStore : 'js/stores/AppStores.js',
        //     ActionType : 'js/actions/ActionType.js',
        //     AppAction : 'js/actions/AppAction.js'
        // }
    },
    node: {
        fs: 'empty'
    },
    externals: [
        {
            "jquery": "jQuery",
            "react": "React",
            "react-dom": "ReactDOM",
            "zepto": "Zepto",
            "amazeui-react": "AMUIReact",
            "marked":"marked",
            "moment":"moment"
        },
        require('webpack-require-http')
    ]
};
